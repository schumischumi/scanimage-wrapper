import sh
import re


def list_devices() -> list:
    """Returns a dictionary with all available devices (scanimage -L)

    Args:
        verbose (bool, optional): verbose output (scanimage --verbose). Defaults to False.

    Returns:
        list: Nested list with all devices. First item of the inner list is the device ID
              and the secend item is the device name
    """

    scanimage_options = ['--formatted-device-list=%d %v %m%n']
    try:
        # device_list = scanimage(scanimage_options)
        device_list = sh.scanimage(scanimage_options)
    except ErrorReturnCode:  # type: ignore # noqa: F821 Will be created dynamically if an error occures
        print("unknown error")

    devices_list = device_list.split("\n")

    # remove last item created by linebreak split
    if devices_list[-1] == '':
        devices_list = devices_list[:-1]

    devices = []
    if len(devices_list) > 0:
        for dev in devices_list:
            dev_split = dev.split(" ")
            device = []
            # append device id
            device.append(dev_split[0])
            # append combined vendor and model name
            device.append(dev_split[1] + " " + dev_split[2])
            devices.append(device)
    return devices


def parse_options(unparsed: str, description: str) -> dict:
    """parses the lines from scanimage --all-options

    Args:
        unparsed (str): unparsed line with parameter
        description (str): concated description

    Returns:
        dict: parsed parameters including the description
    """

    dictionary = {}
    dictionary['parameters'] = {}
    unclear_command = ""
    description = description.strip()
    command = unparsed.lstrip().split(" ")[0]

    # extract options that are connected by an equal sign from command like "--remove-blank[=(yes|no)]"
    if "[=(" in command and command[-2:] == ')]':
        unclear_command = command
        options = command.split("[=(")[1]
        command = command.split("[=(")[0]
        # remove ")]"
        options = options[:-2]
        options = options.split("|")
        dictionary['parameters']['options'] = options
        dictionary['parameters']['option_type'] = "selection"
    # extract range options that are separated by two periods
    elif ".." in unparsed:
        options = unparsed.lstrip().split(" ")[1].split("..")
        dictionary['parameters']['option_type'] = "range"
        # sometimes scanimage adds a unit to the last option like "255.00mm"
        # if thats the case extract it, remove it from the last option and add it to option_unit
        dictionary['parameters']['option_unit'] = re.sub(r'\d', '', options[1]).replace('.', '')
        # remove everything that is no number or period
        options[1] = re.sub('[^0-9\.]', '', options[1])  # noqa: W605 false positive
    # extract options that are separated by a pipe but not connected an equal sign but a whitespace
    elif "|" in unparsed:
        options = unparsed.lstrip().split("|")
        options[0] = options[0].lstrip().replace(command, '').lstrip()
        # because of whitespaces in the option (i.e. "ADF Front") "[" is used to split
        options[-1] = options[-1].split("[")[0].strip()
        dictionary['parameters']['option_type'] = "selection"

        # check if all options are numbers and if they put a unit on the last option
        if options[:-1] == [row for row in options if row.replace(".", '').isdigit()]:
            dictionary['parameters']['option_unit'] = re.sub(r'\d', '', options[-1]).replace('.', '')
            options[-1] = re.sub('[^0-9\.]', '', options[-1])  # noqa: W605 false positive
    # if no options are detected till now, it's assumed that there is only one option
    else:
        unclear_command = unparsed.lstrip().split(" ")[0]
        single_option = unparsed.lstrip().replace(unclear_command, '').split("[")[0].strip()
        options = []
        options.append(single_option)

    dictionary['parameters']['options'] = options

    # extract the default value which is wrapped in square brackets
    default_value = unparsed.lstrip().replace(unclear_command, '').split("[")
    # if the last two segments contain contain square brackets it's assumed that the last segment is an info
    # like "[hardware]" or "[advanced]". That info will be appended to the description
    if "]" in default_value[-2]:
        dictionary['parameters']['default'] = default_value[-2].replace("[", '').replace("]", '').strip()
        description = description + " [" + default_value[-1]
    else:
        dictionary['parameters']['default'] = default_value[-1].replace("[", '').replace("]", '').strip()

    dictionary['parameters']['description'] = description
    dictionary['command'] = command

    return dictionary


def get_options(device_id: str) -> dict:
    """reads and parses the output from scanimage --all-options

    Args:
        device_id (str): ID of the device

    Returns:
        dict: parsed device specific options
    """

    options = {}
    section = ""
    description = ""
    options_unparsed = ""
    parsing = False
    for line in sh.scanimage('--all-options', '-d', device_id, _iter=True):
        line = line.replace("\n", "")
        if 'All options specific to device' in line:
            parsing = True
        elif parsing and line[0:2] == '  ' and line[2] != ' ' and line.strip()[-1] == ":":
            if options_unparsed != "":
                option = parse_options(options_unparsed, description)
                options[section][option['command']] = option['parameters']
                options_unparsed = ""
                description = ""
            section = line[:-1].strip()
            options[section] = {}
        elif parsing and line[0:4] == '    ' and line[4:6] == '--':
            if options_unparsed != "":
                option = parse_options(options_unparsed, description)
                options[section][option['command']] = option['parameters']
                options_unparsed = ""
                description = ""
            description = ""
            options_unparsed = line
        elif parsing and line[0:8] == '        ' and line[8] != ' ':
            if description != "":
                description = description + " " + line
            else:
                description = line

    return options


def start_scan(device_id: str,
               options: list,
               working_dir: str,
               format: str = 'tiff',
               output_file: str = 'scanimage-output.',
               verbose: bool = False,
               buffer_size: int = 32,
               batch: bool = False,
               batch_format: str = 'scanimage-output-%d.',
               batch_start: int = 0,
               batch_increment: int = 1):
    """scans either a single image or multiple in batch mode

    Args:
        device_id (str): ID of the device
        options (list): Device specific options
        working_dir (str): Working directory for output
        format (str, optional): Ouput file format. options are pnm|tiff|png|jpeg|pdf . Defaults to 'tiff'.
        output_file (str, optional): Output filename. Will not be used in batch mode. Defaults to 'modm-raw.'.
        verbose (bool, optional): verbose output (scanimage --verbose). Defaults to False.
        buffer_size (int, optional): Changes the input buffer size from 32KB to the number kB specified or 1M.
                                     Defaults to 32.
        batch (bool, optional): Scans in batch mode. Defaults to False.
        batch_format (str, optional): Filename format for batchmode. Defaults to 'modm-raw-%d.'.
        batch_start (int, optional): Start selects the page number to start naming files with. Defaults to 0.
        batch_increment (int, optional): Increment you can change the amount that the number in the filename
                                         is incremented by. Defaults to 1.
    """

    scanimage_options = [f"--device-name={device_id}",
                         f"--format={format}",
                         f"--buffer-size={buffer_size}"]
    if batch:
        scanimage_options.append(f"--batch={batch_format}{format}")
        scanimage_options.append(f"--batch-start={batch_start}")
        scanimage_options.append(f"--batch-increment={batch_increment}")
    else:
        scanimage_options.append(f"--output-file={output_file}{format}")

    scanimage_options.extend(options)

    if verbose:
        scanimage_options.append("--verbose")
        scanimage_options.append("--batch-print")

    sh.scanimage(scanimage_options, _cwd=working_dir)


# #   example:
# # list available devices
# devices = list_devices()
# print(devices)

# # select second device
# my_scanner = devices[1][0]
# print(my_scanner)

# # get device specific options
# my_scanner_options = get_options(my_scanner)
# print(my_scanner_options)

# # define options for scan
# options = ["--mode", "Color", '--resolution', "200", '--source', 'ADF Duplex']

# # scan
# start_scan(device_id=my_scanner,
#            options=options,
#            working_dir="/home/user/scan/cwd/",
#            verbose=True,
#            batch=True)
