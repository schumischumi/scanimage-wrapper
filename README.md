# scanimage-wrapper

tl;dr: A simple wrapper for the sane commandline tool scanimage.

This motivation for this package was mainly fueled mainly by my canon scanner, which didn't like the package [python-sane](https://pypi.org/project/python-sane/) and I also had trouble with the [GObject approach](https://doc.openpaper.work/libinsane/latest/libinsane/howto_python.html), which is also quite dependency heavy.


So if one of the options above works for you I would advice you to use them, because they are much cleaner and by far more flexible.

This package on the other hand is quite easy to use, because it only uses [sh](https://pypi.org/project/sh/) to wrap the scanimage tool provided by sane.

---

## Requirements

To use this wrapper the command ```scanimage``` is necessary.

On most distributions a package like ```sane-backends``` or ```sane``` will provide it.

### Fedora / RHEL / CentOS
```bash
dnf install sane-backends
```

### Arch
```bash
pacman -S sane
```

### Debian / Ubuntu
```bash
apt install sane-utils
```

## PIP

```bash
pip install scanimage-wrapper
```

## Usage

```python
import scanimage-wrapper

# list available devices
devices = list_devices()
print(devices)

# select second device
my_scanner = devices[1][0]
print(my_scanner)

# get device specific options
my_scanner_options = get_options(my_scanner)
print(my_scanner_options)

# define options for the scan
options = ["--mode", "Color", '--resolution', "200", '--source', 'ADF Duplex']

# scan
start_scan(device_id=my_scanner,
           options=options,
           working_dir="/home/user/scan/cwd/",
           verbose=True,
           batch=True)
```
