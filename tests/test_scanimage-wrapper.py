from unittest.mock import patch
from scanimage_wrapper import scanimage_wrapper


@patch('scanimage_wrapper.scanimage_wrapper.sh.scanimage', create=True)
def test_list_devices(test_patch):
    test_patch.return_value = "device_id:1 COMEON DDR-1990\ndevice_id:2 KP ScanBrett\ndevice_id:3 Fujuju SnapTapIX3\n"  # noqa: E501
    expected_list = [["device_id:1", "COMEON DDR-1990"], ["device_id:2", "KP ScanBrett"], ["device_id:3", "Fujuju SnapTapIX3"]]  # noqa: E501
    assert scanimage_wrapper.list_devices() == expected_list


@patch('scanimage_wrapper.scanimage_wrapper.sh.scanimage', create=True)
def test_get_options(test_patch):
    mock_options_raw = open("./tests/mock_options.txt", "r")
    test_patch.return_value = mock_options_raw.readlines()
    e_dict = {'Standard': {'--source': {'option_type': 'selection', 'options': ['ADF Front', 'ADF Duplex'], 'default': 'ADF Front', 'description': 'Selects the scan source (such as a document-feeder).'}, '--mode': {'option_type': 'selection', 'options': ['Lineart', 'Gray', 'Color'], 'default': 'Gray', 'description': 'Selects the scan mode (e.g., lineart, monochrome, or color).'}, '--resolution': {'option_type': 'selection', 'option_unit': 'dpi', 'options': ['100', '150', '200', '240', '300', '400', '600'], 'default': '600', 'description': 'Sets the resolution of the scanned image.'}}, 'Geometry': {'--page-width': {'option_type': 'range', 'option_unit': 'mm', 'options': ['0', '215.872'], 'default': '215.872', 'description': 'Specifies the width of the media.  Required for automatic centering of         sheet-fed scans.'}, '--page-height': {'option_type': 'range', 'option_unit': 'mm', 'options': ['0', '355.554'], 'default': '279.364', 'description': 'Specifies the height of the media.         Top-left x position of scan area.         Top-left y position of scan area.         Width of scan-area.         Height of scan-area.'}}, 'Enhancement': {'--brightness': {'option_type': 'range', 'option_unit': '', 'options': ['-127', '127'], 'default': '0', 'description': 'Controls the brightness of the acquired image.'}, '--contrast': {'option_type': 'range', 'option_unit': '', 'options': ['-127', '127'], 'default': '0', 'description': 'Controls the contrast of the acquired image.'}, '--threshold': {'option_type': 'range', 'option_unit': '', 'options': ['0', '255'], 'default': 'inactive', 'description': 'Select minimum-brightness to get a white point'}}, 'Advanced': {'--df-thickness': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Detect double feeds using thickness sensor [advanced]'}, '--df-length': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Detect double feeds by comparing document lengths [advanced]'}, '--rollerdeskew': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Request scanner to correct skewed pages mechanically [advanced]'}, '--swdeskew': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Request driver to rotate skewed pages digitally [advanced]'}, '--swdespeck': {'option_type': 'range', 'option_unit': '', 'options': ['0', '9'], 'default': '0', 'description': 'Maximum diameter of lone dots to remove from scan'}, '--swcrop': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Request driver to remove border from pages digitally [advanced]'}, '--swskip': {'option_type': 'range', 'option_unit': '%', 'options': ['0', '100'], 'default': '0', 'description': 'Request driver to discard pages with low percentage of dark pixels'}, '--stapledetect': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Request scanner to halt if stapled pages are detected [advanced]'}, '--dropout-front': {'option_type': 'selection', 'options': ['None', 'Red', 'Green', 'Blue', 'Enhance Red', 'Enhance Green', 'Enhance Blue'], 'default': 'None', 'description': 'One-pass scanners use only one color during gray or binary scanning,         useful for colored paper or ink [advanced]'}, '--dropout-back': {'option_type': 'selection', 'options': ['None', 'Red', 'Green', 'Blue', 'Enhance Red', 'Enhance Green', 'Enhance Blue'], 'default': 'None', 'description': 'One-pass scanners use only one color during gray or binary scanning,         useful for colored paper or ink [advanced]'}, '--buffermode': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Request scanner to read pages async into internal memory [advanced]'}, '--side': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Tells which side (0=front, 1=back) of a duplex scan the next call to         sane_read will return. [read-only]'}}, 'Sensors': {'--start': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Big green or small 1 button [hardware]'}, '--stop': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Small orange or small 2 button [hardware]'}, '--button-3': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Small 3 button [hardware]'}, '--newfile': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'New File button [hardware]'}, '--countonly': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Count Only button [hardware]'}, '--bypassmode': {'options': ['yes', 'no'], 'option_type': 'selection', 'default': 'no', 'description': 'Bypass Mode button [hardware]'}, '--counter': {'option_type': 'range', 'option_unit': '', 'options': ['0', '500'], 'default': '0', 'description': 'Scan counter [hardware]'}, '--roller-counter': {'options': ['<int>'], 'default': '109580', 'description': 'Scans since last roller replacement [hardware]'}}}  # noqa: E501
    assert scanimage_wrapper.get_options('fake_id') == e_dict


@patch('scanimage_wrapper.scanimage_wrapper.sh.scanimage', create=True)
def test_call_scanimage_batch(test_patch):
    options = ["--mode", "Color", '--resolution', "200", '--source', 'ADF Duplex']
    scanimage_wrapper.start_scan(device_id='fake_device_id',
                                 options=options,
                                 working_dir="/home/user/scan/cwd/",
                                 verbose=True,
                                 batch=True)
    expected = (['--device-name=fake_device_id', '--format=tiff', '--buffer-size=32', '--batch=scanimage-output-%d.tiff', '--batch-start=0', '--batch-increment=1', '--mode', 'Color', '--resolution', '200', '--source', 'ADF Duplex', '--verbose', '--batch-print'],)  # noqa: E501
    assert scanimage_wrapper.sh.scanimage.call_args.args == expected
    expected = {'_cwd': '/home/user/scan/cwd/'}
    assert scanimage_wrapper.sh.scanimage.call_args.kwargs == expected


@patch('scanimage_wrapper.scanimage_wrapper.sh.scanimage', create=True)
def test_call_scanimage_single(test_patch):
    options = ["--mode", "Color", '--resolution', "200", '--source', 'ADF Duplex']
    scanimage_wrapper.start_scan(device_id='fake_device_id',
                                 options=options,
                                 working_dir="/home/user/scan/cwd/")
    expected = (['--device-name=fake_device_id', '--format=tiff', '--buffer-size=32', '--output-file=scanimage-output.tiff', '--mode', 'Color', '--resolution', '200', '--source', 'ADF Duplex'],)  # noqa: E501
    assert scanimage_wrapper.sh.scanimage.call_args.args == expected
    expected = {'_cwd': '/home/user/scan/cwd/'}
    assert scanimage_wrapper.sh.scanimage.call_args.kwargs == expected
